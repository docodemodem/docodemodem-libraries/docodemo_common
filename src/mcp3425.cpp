/*
 * AD converter driver for MCP3422/3/4/5
 * mcp3425.cpp
 *
 * Copyright (c) 2020 Circuit Desgin,Inc
 * Released under the MIT license
 */

#include "mcp3425.h"
#define MCP3425_configRegister 0b10001000 //16bit 15sps Oneshot PGA x1

MCP3425::MCP3425(void){
}

bool MCP3425::begin(uint8_t addr, TwoWire &theWire)
{
    i2c_addr = addr;
    _wire = &theWire;

    return true;
}

float MCP3425::readADC(uint8_t channel)
{
    if(channel > 3)
    {
        return 0;
    }
    _wire->beginTransmission(i2c_addr);
    _wire->write(MCP3425_configRegister | (channel<<5)); //OneShot 16bit 15sps
    _wire->endTransmission();

    delay(70);//wait 15sps

    _wire->requestFrom(i2c_addr, (uint8_t)2);
    int16_t data = (_wire->read() << 8) + _wire->read();

    return (float)data * Vref / 32767.0;
}

void MCP3425::startMeasure(uint8_t channel)
{
    if (channel > 3)
    {
        return;
    }
    _wire->beginTransmission(i2c_addr);
    _wire->write(MCP3425_configRegister | (channel << 5)); //OneShot 16bit 15sps
    _wire->endTransmission();
}

float MCP3425::readResult()
{
    _wire->requestFrom(i2c_addr, (uint8_t)2);
    int16_t data = (_wire->read() << 8) + _wire->read();

    return (float)data * Vref / 32767.0;
}
