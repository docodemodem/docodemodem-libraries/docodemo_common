//
// SlrModem.h
//
// (c) 2019 Reimesch Kommunikationssysteme
// Authors: aj, cl
// Created on: 13.03.2019
// Released under the MIT license
//
// Interface driver to Circuit Design SLR/MLR modems.
// The driver functionality only covers the serial interface - no I/O, Reset etc.

#pragma once
#include "docodemo.h"
#ifdef _DOCODE_MODEM_MINI
#include <FreeRTOS_SAMD21.h>
#endif

// Serial command messages
enum class SlrModemResponse
{
    // internal state of modem
    Idle,       // no message received or expected
    ParseError, // garbage characters Received
    Timeout,    // no response received

    // serial commands
    ShowMode,          // e.g. "FSK BIN MODE" or "LORA CMD MODE"
    SaveValue,         // "*WR=PS"  : Value written
    Channel,           // "*CH..."  : Set frequency channel
    SerialNumber,      // "*SN=..." : Acquire serial number of the device
    SlrModem_DtAck,    // @DT command request accepted
    SlrModem_DtIr,     // @DT command IR message (LoRa only)
    DataReceived,      // "*DR=..." : Data received
    RssiLastRx,        // "*RS=..." : Acquire RSSI for the last data received
    RssiCurrentChannel // "*RA=..." : Acquire the current RSSI value
};

// API level error codes
enum class SlrModemError
{
    Ok,         // no error
    Busy,       // waiting for response
    InvalidArg, // command has invalid argument
    FailLbt,    // Transmit failed because of LBT (channel occupied)
    Fail        // an error occurred
};

enum class SlrModemMode : uint8_t
{
    FskBin = 0,
    FskCmd = 1,
    LoRaBin = 2,
    LoRaCmd = 3,
};

enum class SlrModemSpreadFactor : uint8_t
{
    Chips128 = 0,
    Chips256 = 1,
    Chips512 = 2,
    Chips1024 = 3,
    Chips2048 = 4,
    Chips4096 = 5,
};

// "high-level" cmd state
enum class SlrModemCmdState
{
    Parsing = 0,         // still parsing, waiting for further input
    Garbage,             // garbage received
    Overflow,            // too many characters received
    FinishedCmdResponse, // received a command that might be syntactically correct
    FinishedDrResponse,  // received a dr response
};

// "low-level" parser states
enum class SlrModemParserState
{
    Start = 0,

    ReadCmdFirstLetter,  // first char had been '*', now read first letter of command
    ReadCmdSecondLetter, // now read second letter of command
    ReadCmdParam,        // so far '*XX' has been read, now read param of command (might be started by '=')

    ReadRawString,

    RadioDrSize,       // special case for DR telegram -> wait for length information
    RadioDrSkipAdress, // TODO REMOVE, only for MLR test
    RadioDrPayload,    // wait for payload to finish

    ReadCmdUntilCR, // wait for '\r' at end of command
    ReadCmdUntilLF, // wait for '\n' at end of command

    /* TODO discuss stay at one level of parser state
   Garbage,              // garbage received
   Overflow,             // too many characters received
   FinishedCmdResponse,  // received a command that might be syntactically correct
   FinishedDrResponse,   // received a dr response
   */
};

/**
 * \brief Callback for async calls and Radio Message Received events.
 * \param error - Status of the received response, if value != SlrModemError::Ok, all following fields are invalid
 * \param responseType - Type of the received response 
 * \param value - the returned value of GetRssiCurrentChannelAsync, TransmitDataAsync or GetRssiCurrentChannelAsync, else 0
 * \param pPayload - the received radio payload in case of reponseType SlrModemResponse::DataReceived 
 * \param len - the received radio payload lenｴin case of reponseType SlrModemResponse::DataReceived 
 * 
 */
typedef void (*SlrModem_AsyncCallback)(SlrModemError error, SlrModemResponse responseType, int32_t value, const uint8_t *pPayload, uint16_t len);

// instance data structure
class SlrModem
{

public: // methods
    // TODO: Description
    void Init(HardwareSerial &pUart, SlrModem_AsyncCallback pCallback);

    // sets the async callback. If set to nullptr, no callback will take place.
    void SetAsyncCallback(SlrModem_AsyncCallback pCallback) { m_pCallback = pCallback; }

    // TODO: Description
    SlrModemError GetMode(SlrModemMode *pMode);
    SlrModemError SetMode(SlrModemMode mode, bool saveValue);

    // TODO: Description
    SlrModemError GetSpreadFactor(SlrModemSpreadFactor *pSf);
    SlrModemError SetSpreadFactor(SlrModemSpreadFactor sf, bool saveValue);

    // TODO: Description
    SlrModemError TransmitData(const uint8_t *pMsg, uint8_t len);

    // Get/set the frequency channel (0x07 <= channel <= 0x2E)
    SlrModemError GetChannel(uint8_t *pChannel);
    SlrModemError SetChannel(uint8_t channel, bool saveValue);
	
	// Get/set the Group ID (0x00 <= GI <= 0xFF)
    SlrModemError GetGroupID(uint8_t *pGI);
    SlrModemError SetGroupID(uint8_t gi, bool saveValue);
	
	// Get/set the Destination ID (0x00 <= DI <= 0xFF)
    SlrModemError GetDestinationID(uint8_t *pDI);
    SlrModemError SetDestinationID(uint8_t di, bool saveValue);

    // Get/set the Equipment ID (0x00 <= EI <= 0xFF)
    SlrModemError GetEquipmentID(uint8_t *pEI);
    SlrModemError SetEquipmentID(uint8_t ei, bool saveValue);
	
    // TODO: Description
    SlrModemError GetRssiLastRx(int16_t *pRssi);

    // TODO: Description
    SlrModemError GetRssiCurrentChannel(int16_t *pRssi);
    SlrModemError GetRssiCurrentChannelAsync();

    // Get/set the contact function for DIO1..DIO8.
    SlrModemError GetContactFunction(uint8_t *pContactFunction);
    SlrModemError SetContactFunction(uint8_t contactFunction, bool saveValue);

    // TODO: Description
    SlrModemError GetSerialNumber(uint32_t *pSn);
    SlrModemError GetSerialNumberAsync();

    // Returns true if the modem received a radio telegram ready for collection.
    bool HasPacket() { return m_drMessagePresent; }

    // Receive the packet and calls callback.
    SlrModemError GetPacket(const uint8_t **ppData, uint8_t *len);

    // remove the received packet
    void DeletePacket() { m_drMessagePresent = false; }

    // returns the associated Stream
    //RK::IStream *GetStream() { return m_pUart; }

    // TODO: Description
    void Work();

private: // methods
    // mock-up for timeout check, required later on to prevent infinite loop on data error
    bool m_IsTimeout() {
        if (!bTimeout && millis() - startTime > timeOut)
        {
            bTimeout = true;
        }
        return bTimeout;
    }
    bool bTimeout = true;
    uint32_t startTime;
    uint32_t timeOut;
    // mock-up for restarting timeout counter
    void m_StartTimeout(uint32_t ms = 500) {
        bTimeout = false;
        startTime = millis();
        timeOut = ms;
    }

    // wrtie string to UART
    void m_WriteString(const char *pString);

    // methods for reading from UART, using a one-byte buffer
    uint8_t m_ReadByte();
    void m_UnreadByte(uint8_t unreadByte);
    void m_ClearUnreadByte();
    //uint32_t m_CountReadable();
    uint32_t m_Read(uint8_t *pDst, uint32_t count);

    // Set the parser to the initial state, withdrawing any data that is currently parsed
    void m_ResetParser();

    void m_FlushGarbage();

    // TODO: Description
    SlrModemCmdState m_Parse();

    // TODO: Description
    SlrModemError m_WaitCmdResponse(uint32_t ms = 500);

    // TODO: Description
    void m_SetExpectedResponses(SlrModemResponse ep0, SlrModemResponse ep1, SlrModemResponse ep2);

    // dispatch a received radio message
    // if a response contains a value it is stored to pParam.
    SlrModemError m_DispatchCmdResponseAsync();

    // check if the received message is equal to "*WR=PS"
    SlrModemError m_HandleMessage_WR();

    SlrModemError m_GetHexByte(uint8_t *pValue, const char *commandString, uint32_t responseLen, const char *responsePrefix);

    // helper method for reponses that contain a one-byte hex value
    SlrModemError m_HandleMessageHexByte(uint8_t *pValue, uint32_t responseLen, const char *responsePrefix);

    // check if the received message is of RS format and fill in the RSSI
    SlrModemError m_HandleMessage_RS(int16_t *pRssi);

    // check if the received message is of RA format and fill in the RSSI
    SlrModemError m_HandleMessage_RA(int16_t *pRssi);

    // check if the received message is of SN format and fill in the serial number
    SlrModemError m_HandleMessage_SN(uint32_t *pSerialNumber);

    void m_ClearOneLine();

private: // data
    SemaphoreHandle_t xSLRSemaphore;

    HardwareSerial *m_pUart;
    SlrModemResponse m_asyncExpectedResponse;
    SlrModemResponse m_asyncExpectedResponses[3];
    SlrModemParserState m_parserState;

    // receive buffer and index for modem response / data reception
    int16_t m_oneByteBuf; // 1-byte buffer that is needed if parser has to "unread" a byte; value == -1 if buffer empty
    uint16_t m_rxIdx;
    uint8_t m_rxMessage[32];

    // special receive buffer and data for '@DR' command
    bool m_drMessagePresent;
    uint8_t m_drMessageLen;
    uint8_t m_drMessage[256];
    SlrModemMode m_mode;
    SlrModem_AsyncCallback m_pCallback;
};
