//
// SlrModem.cpp
//
// (c) 2019 Reimesch Kommunikationssysteme
// Authors: aj, cl
// Created on: 13.03.2019
// Released under the MIT license
//
// Interface driver to Circuit Design SLR/MLR modems.
//

#include "SlrModem.h"
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <array>

//#define SLRMODEM_SKIP_ADRESS_FOR_MLR

// string codes for SLR modem
// write value
#define SLR_WRITE_VALUE_RESPONSE_PREFIX "*WR=PS"
#define SLR_WRITE_VALUE_RESPONSE_LEN 6 // length of "*WR=PS" excluding "\r\n"

// get/set modem mode (i.e.  FSK <-> LoRa and command <-> binary)
#define SLR_GET_MODE_STRING "@MO\r\n"
#define SLR_SET_MODE_PREFIX_STRING "@MO"
#define SLR_SET_MODE_RESPONSE_PREFIX "*MO="
#define SLR_SET_MODE_RESPONSE_LEN 6 // length of "*MO=01" excluding "\r\n"

// get/set modem mode (i.e.  FSK <-> LoRa and command <-> binary)
#define SLR_GET_SF_STRING "@SF\r\n"
#define SLR_SET_SF_PREFIX_STRING "@SF"
#define SLR_SET_SF_RESPONSE_PREFIX "*SF="
#define SLR_SET_SF_RESPONSE_LEN 6

// transmit radio packet
#define SLR_TRANSMISSION_PREFIX_STRING "@DT"
#define SLR_TRANSMISSION_RESPONSE_PREFIX "*DT="
#define SLR_TRANSMISSION_RESPONSE_LEN 6 // length of "*DT=06" excluding "\r\n"

// information responses
#define SLR_INFORMATION_RESPONSE_PREFIX "*IR="
#define SLR_INFORMATION_RESPONSE_LEN 6             // length of "*IR=03" excluding "\r\n"
#define SLR_INFORMATION_RESPONSE_ERR_NO_TX 1       // data transmission is not possible (for unknon reasons)
#define SLR_INFORMATION_RESPONSE_ERR_OTHER_WAVES 2 // data transmission is not possible because of presence of other LoRa modules
#define SLR_INFORMATION_RESPONSE_ERR_OK 3          // data transmission complete

// get/set channel frequency
#define SLR_GET_CHANNEL_STRING "@CH\r\n"
#define SLR_SET_CHANNEL_PREFIX_STRING "@CH"
#define SLR_SET_CHANNEL_RESPONSE_PREFIX "*CH="
#define SLR_SET_CHANNEL_RESPONSE_LEN 6 // length of "*CH=0E" excluding "\r\n"
#define SLR_SET_CHANNEL_MIN_VALUE_JP 0x07 // channel 7
#define SLR_SET_CHANNEL_MAX_VALUE_JP 0x2E // channel 46

// get/set group ID
#define SLR_GET_GROUP_STRING "@GI\r\n"
#define SLR_SET_GROUP_PREFIX_STRING "@GI"
#define SLR_SET_GROUP_RESPONSE_PREFIX "*GI="
#define SLR_SET_GROUP_RESPONSE_LEN 6 // length of "*GI=0E" excluding "\r\n"

// get/set destination ID
#define SLR_GET_DESTINATION_STRING "@DI\r\n"
#define SLR_SET_DESTINATION_PREFIX_STRING "@DI"
#define SLR_SET_DESTINATION_RESPONSE_PREFIX "*DI="
#define SLR_SET_DESTINATION_RESPONSE_LEN 6 // length of "*DI=0E" excluding "\r\n"

// get/set equipment ID
#define SLR_GET_EQUIPMENT_STRING "@EI\r\n"
#define SLR_SET_EQUIPMENT_PREFIX_STRING "@EI"
#define SLR_SET_EQUIPMENT_RESPONSE_PREFIX "*EI="
#define SLR_SET_EQUIPMENT_RESPONSE_LEN 6 // length of "*EI=0E" excluding "\r\n"

// get RSSI value of last data received
#define SLR_GET_RSSI_LAST_RX_STRING "@RS\r\n"
#define SLR_GET_RSSI_LAST_RX_RESPONSE_PREFIX "*RS="
#define SLR_GET_RSSI_LAST_RX_RESPONSE_MIN_LEN 10 // length of "*RS=-12dBm" excluding "\r\n"
#define SLR_GET_RSSI_LAST_RX_RESPONSE_MAX_LEN 11 // length of "*RS=-123dBm" excluding "\r\n"

// get current RSSI value of channel set
#define SLR_GET_RSSI_CURRENT_CHANNEL_STRING "@RA\r\n"
#define SLR_GET_RSSI_CURRENT_CHANNEL_RESPONSE_PREFIX "*RA="
#define SLR_GET_RSSI_CURRENT_CHANNEL_RESPONSE_MIN_LEN 10 // length of "*RA=-12dBm" excluding "\r\n"
#define SLR_GET_RSSI_CURRENT_CHANNEL_RESPONSE_MAX_LEN 11 // length of "*RA=-123dBm" excluding "\r\n"

// get/set contact function
#define SLR_GET_CONTACT_FUNCTION_STRING "@PS\r\n"
#define SLR_SET_CONTACT_FUNCTION_PREFIX_STRING "@PS"
#define SLR_SET_CONTACT_FUNCTION_RESPONSE_PREFIX "*PS="
#define SLR_SET_CONTACT_FUNCTION_RESPONSE_LEN 6 // length of "*PS=0F" excluding "\r\n"

// get serial number
#define SLR_GET_SERIAL_NUMBER_STRING "@SN\r\n"
#define SLR_GET_SERIAL_NUMBER_RESPONSE_PREFIX "*SN="
#define SLR_GET_SERIAL_NUMBER_RESPONSE_LEN 12 // length of "*SN=12345679" excluding "\r\n"

const TickType_t xTicksToWait = 500UL;

// string length calculated at compile time
template <uint16_t N>

uint16_t static_strlen(const char (&cstr)[N])
{
    for (uint16_t i = 0; i < N; i++)
    {
        if (cstr[i] == 0)
            return i;
    }

    return 0xFFFF;
}

static bool s_ParseHex(const uint8_t *pData, uint8_t len, uint32_t *pResult)
{
    *pResult = 0;

    while (len--)
    {
        *pResult <<= 4;
        if (isxdigit(*pData))
        {
            if (*pData >= 'a')
            {
                *pResult |= *pData - 'a' + 10;
            }
            else if (*pData >= 'A')
            {
                *pResult |= *pData - 'A' + 10;
            }
            else
            {
                *pResult |= *pData - '0';
            }
        }
        else
        {
            return false;
        }

        ++pData;
    }

    return true;
}

static bool s_ParseDec(const uint8_t *pData, uint8_t len, uint32_t *pResult)
{
    *pResult = 0;

    while (len--)
    {
        *pResult = *pResult * 10;
        if (isdigit(*pData))
        {
            *pResult += *pData - '0';
        }
        else
        {
            return false;
        }

        ++pData;
    }

    return true;
}

void SlrModem::Init(HardwareSerial &pUart, SlrModem_AsyncCallback pCallback)
{
    xSLRSemaphore = xSemaphoreCreateBinary();
    configASSERT(xSLRSemaphore);
    xSemaphoreGive(xSLRSemaphore);

    m_asyncExpectedResponse = SlrModemResponse::Idle;
    m_pCallback = pCallback;
    m_pUart = &pUart;
    m_rxIdx = 0;
    m_parserState = SlrModemParserState::Start;
    m_drMessagePresent = false;
    m_drMessageLen = 0;
    m_ResetParser();
    GetMode(&m_mode);
}

SlrModemError SlrModem::GetMode(SlrModemMode *pMode)
{
    if (m_asyncExpectedResponse != SlrModemResponse::Idle)
    {
        return SlrModemError::Busy;
    }

    SlrModemError rv = SlrModemError::Fail;
    if (xSemaphoreTake(xSLRSemaphore, xTicksToWait) == pdTRUE)
    {
        m_WriteString(SLR_GET_MODE_STRING);

        rv = m_WaitCmdResponse();

        if (rv == SlrModemError::Ok)
        {
            rv = m_HandleMessageHexByte(reinterpret_cast<uint8_t *>(pMode), SLR_SET_MODE_RESPONSE_LEN, SLR_SET_MODE_RESPONSE_PREFIX);
        }
        xSemaphoreGive(xSLRSemaphore);
    }

    return rv;
}

SlrModemError SlrModem::SetMode(SlrModemMode mode, bool saveValue)
{
    if (mode == SlrModemMode::FskBin || mode == SlrModemMode::LoRaBin)
    {
        // binary modes currently not supported
        return SlrModemError::InvalidArg;
    }

    if (m_asyncExpectedResponse != SlrModemResponse::Idle)
    {
        return SlrModemError::Busy;
    }

    SlrModemError rv = SlrModemError::Fail;
    if (xSemaphoreTake(xSLRSemaphore, xTicksToWait) == pdTRUE)
    {
        // e.g. "@MO01\r\n" or "@MO01/W\r\n"
        m_WriteString(SLR_SET_MODE_PREFIX_STRING);
        std::array<char, 7> numStr;
        snprintf(numStr.data(), numStr.size(), "%02X%s\r\n", static_cast<unsigned>(mode), (saveValue ? ("/W") : ("")));
        m_WriteString(numStr.data());

        rv = m_WaitCmdResponse();
        if (rv == SlrModemError::Ok && saveValue)
        {
            rv = m_HandleMessage_WR();
            if (rv == SlrModemError::Ok)
            {
                rv = m_WaitCmdResponse();
            }
        }

        SlrModemMode modeResponse{};
        if (rv == SlrModemError::Ok)
        {
            rv = m_HandleMessageHexByte(reinterpret_cast<uint8_t *>(&modeResponse), SLR_SET_MODE_RESPONSE_LEN, SLR_SET_MODE_RESPONSE_PREFIX);
        }

        if (rv == SlrModemError::Ok)
        {
            rv = (modeResponse == mode) ? SlrModemError::Ok : SlrModemError::Fail;
        }

        if (rv == SlrModemError::Ok)
        {
            m_mode = mode;
            //Handle messages "FSK CMD MODE" etc.
            m_ClearOneLine();
        }

        xSemaphoreGive(xSLRSemaphore);
    }
    return rv;
}

SlrModemError SlrModem::GetSpreadFactor(SlrModemSpreadFactor *pSf)
{
    if (m_asyncExpectedResponse != SlrModemResponse::Idle)
    {
        return SlrModemError::Busy;
    }

    SlrModemError rv = SlrModemError::Fail;
    if (xSemaphoreTake(xSLRSemaphore, xTicksToWait) == pdTRUE)
    {
        m_WriteString(SLR_GET_SF_STRING);

        rv = m_WaitCmdResponse();

        if (rv == SlrModemError::Ok)
        {
            rv = m_HandleMessageHexByte(reinterpret_cast<uint8_t *>(pSf), SLR_SET_SF_RESPONSE_LEN, SLR_SET_SF_RESPONSE_PREFIX);
        }
        xSemaphoreGive(xSLRSemaphore);
    }
    return rv;
}

SlrModemError SlrModem::SetSpreadFactor(SlrModemSpreadFactor sf, bool saveValue)
{
    if (m_asyncExpectedResponse != SlrModemResponse::Idle)
    {
        return SlrModemError::Busy;
    }

    SlrModemError rv = SlrModemError::Fail;
    if (xSemaphoreTake(xSLRSemaphore, xTicksToWait) == pdTRUE)
    {
        // e.g. "@MO01\r\n" or "@MO01/W\r\n"
        m_WriteString(SLR_SET_SF_PREFIX_STRING);
        std::array<char, 7> numStr;
        snprintf(numStr.data(), numStr.size(), "%02X%s\r\n", static_cast<unsigned>(sf), (saveValue ? ("/W") : ("")));
        m_WriteString(numStr.data());

        rv = m_WaitCmdResponse();
        if (rv == SlrModemError::Ok && saveValue)
        {
            rv = m_HandleMessage_WR();
            if (rv == SlrModemError::Ok)
            {
                rv = m_WaitCmdResponse();
            }
        }

        SlrModemSpreadFactor sfResponse{};
        if (rv == SlrModemError::Ok)
        {
            rv = m_HandleMessageHexByte(reinterpret_cast<uint8_t *>(&sfResponse), SLR_SET_SF_RESPONSE_LEN, SLR_SET_SF_RESPONSE_PREFIX);
        }

        if (rv == SlrModemError::Ok)
        {
            rv = (sfResponse == sf) ? SlrModemError::Ok : SlrModemError::Fail;
        }
        xSemaphoreGive(xSLRSemaphore);
    }
    return rv;
}

SlrModemError SlrModem::TransmitData(const uint8_t *pMsg, uint8_t len)
{
    if (m_asyncExpectedResponse != SlrModemResponse::Idle)
    {
        return SlrModemError::Busy;
    }

    SlrModemError rv = SlrModemError::Fail;
    if (xSemaphoreTake(xSLRSemaphore, xTicksToWait) == pdTRUE)
    {
        m_WriteString(SLR_TRANSMISSION_PREFIX_STRING);
        std::array<char, 3> numStr;
        snprintf(numStr.data(), numStr.size(), "%02X", static_cast<unsigned>(len));
        m_WriteString(numStr.data());
        m_pUart->write(pMsg, len);
        m_WriteString("\r\n");

        rv = m_WaitCmdResponse();

        // check transmission response
        uint8_t transmissionResponse{};
        if (rv == SlrModemError::Ok)
        {
            rv = m_HandleMessageHexByte(&transmissionResponse, SLR_TRANSMISSION_RESPONSE_LEN, SLR_TRANSMISSION_RESPONSE_PREFIX);
        }

        // check if length of data in response is correct
        if (rv == SlrModemError::Ok && transmissionResponse != len)
        {
            rv = SlrModemError::Fail;
        }

        // check information response
        if (rv == SlrModemError::Ok)
        {
            if (m_mode == SlrModemMode::LoRaCmd){
                rv = m_WaitCmdResponse(15000);
            }else{
                rv = m_WaitCmdResponse(11);
            }
        }

        // check if transmission has been completed
        uint8_t informationResponse{};
        if (m_mode == SlrModemMode::LoRaCmd)
        {
            if (rv == SlrModemError::Ok)
            {
                rv = m_HandleMessageHexByte(&informationResponse, SLR_INFORMATION_RESPONSE_LEN, SLR_INFORMATION_RESPONSE_PREFIX);
            }

            if (rv != SlrModemError::Ok)
            {
                rv = SlrModemError::Fail;
            }
            else
            {
                switch (informationResponse)
                {
                case SLR_INFORMATION_RESPONSE_ERR_OTHER_WAVES:
                case SLR_INFORMATION_RESPONSE_ERR_NO_TX:
                    rv = SlrModemError::FailLbt;
                    break;
                default:
                    break;
                }
            }
        }
        else
        {
            //FSK mode:
            // if send OK ,no *IR response, carrier sense error make *IR=01
            if (rv != SlrModemError::Ok)
            {
                //timeout mean send ok!
                rv = SlrModemError::Ok;
            }else{
                rv = m_HandleMessageHexByte(&informationResponse, SLR_INFORMATION_RESPONSE_LEN, SLR_INFORMATION_RESPONSE_PREFIX);
                if (rv != SlrModemError::Ok)
                {
                    rv = SlrModemError::Fail;
                }
                else
                {
                    switch (informationResponse)
                    {
                    case SLR_INFORMATION_RESPONSE_ERR_NO_TX:
                        rv = SlrModemError::FailLbt;
                        break;
                    default:
                        break;
                    }
                }
            }
        }
        xSemaphoreGive(xSLRSemaphore);
    }
    return rv;
}

SlrModemError SlrModem::GetChannel(uint8_t *pChannel)
{
    if (m_asyncExpectedResponse != SlrModemResponse::Idle)
    {
        return SlrModemError::Busy;
    }

    SlrModemError rv = SlrModemError::Fail;
    if (xSemaphoreTake(xSLRSemaphore, xTicksToWait) == pdTRUE)
    {
        m_WriteString(SLR_GET_CHANNEL_STRING);

        rv = m_WaitCmdResponse();

        if (rv == SlrModemError::Ok)
        {
            rv = m_HandleMessageHexByte(pChannel, SLR_SET_CHANNEL_RESPONSE_LEN, SLR_SET_CHANNEL_RESPONSE_PREFIX);
        }
        xSemaphoreGive(xSLRSemaphore);
    }
    return rv;
}

SlrModemError SlrModem::SetChannel(uint8_t channel, bool saveValue)
{
    if ((channel < SLR_SET_CHANNEL_MIN_VALUE_JP) || (channel > SLR_SET_CHANNEL_MAX_VALUE_JP))
    {
        return SlrModemError::InvalidArg;
    }

    if (m_asyncExpectedResponse != SlrModemResponse::Idle)
    {
        return SlrModemError::Busy;
    }

    SlrModemError rv = SlrModemError::Fail;
    if (xSemaphoreTake(xSLRSemaphore, xTicksToWait) == pdTRUE)
    {
        // e.g. @CH0A\r\n or @CH45/W\r\n
        m_WriteString(SLR_SET_CHANNEL_PREFIX_STRING);
        std::array<char, 7> numStr;
        snprintf(numStr.data(), numStr.size(), "%02X%s\r\n", static_cast<unsigned>(channel), (saveValue ? ("/W") : ("")));
        m_WriteString(numStr.data());

        rv = m_WaitCmdResponse();
        if (rv == SlrModemError::Ok && saveValue)
        {
            rv = m_HandleMessage_WR();
            if (rv == SlrModemError::Ok)
            {
                rv = m_WaitCmdResponse();
            }
        }

        uint8_t channelResponse{};
        if (rv == SlrModemError::Ok)
        {
            rv = m_HandleMessageHexByte(&channelResponse, SLR_SET_CHANNEL_RESPONSE_LEN, SLR_SET_CHANNEL_RESPONSE_PREFIX);
        }

        if (rv == SlrModemError::Ok && channelResponse != channel)
        {
            rv = SlrModemError::Fail;
        }
        xSemaphoreGive(xSLRSemaphore);
    }
    return rv;
}

SlrModemError SlrModem::GetGroupID(uint8_t *pGI)
{
    if (m_asyncExpectedResponse != SlrModemResponse::Idle)
    {
        return SlrModemError::Busy;
    }

    SlrModemError rv = SlrModemError::Fail;
    if (xSemaphoreTake(xSLRSemaphore, xTicksToWait) == pdTRUE)
    {
        m_WriteString(SLR_GET_GROUP_STRING);

        rv = m_WaitCmdResponse();

        if (rv == SlrModemError::Ok)
        {
            rv = m_HandleMessageHexByte(pGI, SLR_SET_GROUP_RESPONSE_LEN, SLR_SET_GROUP_RESPONSE_PREFIX);
        }
        xSemaphoreGive(xSLRSemaphore);
    }
    return rv;
}

SlrModemError SlrModem::SetGroupID(uint8_t gi, bool saveValue)
{
    if (m_asyncExpectedResponse != SlrModemResponse::Idle)
    {
        return SlrModemError::Busy;
    }

    SlrModemError rv = SlrModemError::Fail;
    if (xSemaphoreTake(xSLRSemaphore, xTicksToWait) == pdTRUE)
    {
        // e.g. @GI0A\r\n or @GI0A/W\r\n
        m_WriteString(SLR_SET_GROUP_PREFIX_STRING);
        std::array<char, 7> numStr;
        snprintf(numStr.data(), numStr.size(), "%02X%s\r\n", static_cast<unsigned>(gi), (saveValue ? ("/W") : ("")));
        m_WriteString(numStr.data());

        rv = m_WaitCmdResponse();
        if (rv == SlrModemError::Ok && saveValue)
        {
            rv = m_HandleMessage_WR();
            if (rv == SlrModemError::Ok)
            {
                rv = m_WaitCmdResponse();
            }
        }

        uint8_t giResponse{};
        if (rv == SlrModemError::Ok)
        {
            rv = m_HandleMessageHexByte(&giResponse, SLR_SET_GROUP_RESPONSE_LEN, SLR_SET_GROUP_RESPONSE_PREFIX);
        }

        if (rv == SlrModemError::Ok && giResponse != gi)
        {
            rv = SlrModemError::Fail;
        }
        xSemaphoreGive(xSLRSemaphore);
    }
    return rv;
}

SlrModemError SlrModem::GetDestinationID(uint8_t *pDI)
{
    if (m_asyncExpectedResponse != SlrModemResponse::Idle)
    {
        return SlrModemError::Busy;
    }

    SlrModemError rv = SlrModemError::Fail;
    if (xSemaphoreTake(xSLRSemaphore, xTicksToWait) == pdTRUE)
    {
        m_WriteString(SLR_GET_DESTINATION_STRING);

        rv = m_WaitCmdResponse();

        if (rv == SlrModemError::Ok)
        {
            rv = m_HandleMessageHexByte(pDI, SLR_SET_DESTINATION_RESPONSE_LEN, SLR_SET_DESTINATION_RESPONSE_PREFIX);
        }
        xSemaphoreGive(xSLRSemaphore);
    }
    return rv;
}

SlrModemError SlrModem::SetDestinationID(uint8_t di, bool saveValue)
{
    if (m_asyncExpectedResponse != SlrModemResponse::Idle)
    {
        return SlrModemError::Busy;
    }

    SlrModemError rv = SlrModemError::Fail;
    if (xSemaphoreTake(xSLRSemaphore, xTicksToWait) == pdTRUE)
    {
        // e.g. @DI0A\r\n or @DI0A/W\r\n
        m_WriteString(SLR_SET_DESTINATION_PREFIX_STRING);
        std::array<char, 7> numStr;
        snprintf(numStr.data(), numStr.size(), "%02X%s\r\n", static_cast<unsigned>(di), (saveValue ? ("/W") : ("")));
        m_WriteString(numStr.data());

        rv = m_WaitCmdResponse();
        if (rv == SlrModemError::Ok && saveValue)
        {
            rv = m_HandleMessage_WR();
            if (rv == SlrModemError::Ok)
            {
                rv = m_WaitCmdResponse();
            }
        }

        uint8_t diResponse{};
        if (rv == SlrModemError::Ok)
        {
            rv = m_HandleMessageHexByte(&diResponse, SLR_SET_DESTINATION_RESPONSE_LEN, SLR_SET_DESTINATION_RESPONSE_PREFIX);
        }

        if (rv == SlrModemError::Ok && diResponse != di)
        {
            rv = SlrModemError::Fail;
        }
        xSemaphoreGive(xSLRSemaphore);
    }
    return rv;
}

SlrModemError SlrModem::GetEquipmentID(uint8_t *pEI)
{
    if (m_asyncExpectedResponse != SlrModemResponse::Idle)
    {
        return SlrModemError::Busy;
    }

    SlrModemError rv = SlrModemError::Fail;
    if (xSemaphoreTake(xSLRSemaphore, xTicksToWait) == pdTRUE)
    {
        m_WriteString(SLR_GET_EQUIPMENT_STRING);

        rv = m_WaitCmdResponse();

        if (rv == SlrModemError::Ok)
        {
            rv = m_HandleMessageHexByte(pEI, SLR_SET_EQUIPMENT_RESPONSE_LEN, SLR_SET_EQUIPMENT_RESPONSE_PREFIX);
        }
        xSemaphoreGive(xSLRSemaphore);
    }
    return rv;
}

SlrModemError SlrModem::SetEquipmentID(uint8_t ei, bool saveValue)
{
    if (m_asyncExpectedResponse != SlrModemResponse::Idle)
    {
        return SlrModemError::Busy;
    }

    SlrModemError rv = SlrModemError::Fail;
    if (xSemaphoreTake(xSLRSemaphore, xTicksToWait) == pdTRUE)
    {
        // e.g. @EI0A\r\n or @EI0A/W\r\n
        m_WriteString(SLR_SET_EQUIPMENT_PREFIX_STRING);
        std::array<char, 7> numStr;
        snprintf(numStr.data(), numStr.size(), "%02X%s\r\n", static_cast<unsigned>(ei), (saveValue ? ("/W") : ("")));
        m_WriteString(numStr.data());

        rv = m_WaitCmdResponse();
        if (rv == SlrModemError::Ok && saveValue)
        {
            rv = m_HandleMessage_WR();
            if (rv == SlrModemError::Ok)
            {
                rv = m_WaitCmdResponse();
            }
        }

        uint8_t eiResponse{};
        if (rv == SlrModemError::Ok)
        {
            rv = m_HandleMessageHexByte(&eiResponse, SLR_SET_EQUIPMENT_RESPONSE_LEN, SLR_SET_EQUIPMENT_RESPONSE_PREFIX);
        }

        if (rv == SlrModemError::Ok && eiResponse != ei)
        {
            rv = SlrModemError::Fail;
        }
        xSemaphoreGive(xSLRSemaphore);
    }
    return rv;
}

SlrModemError SlrModem::GetRssiLastRx(int16_t *pRssi)
{
    if (m_asyncExpectedResponse != SlrModemResponse::Idle)
    {
        return SlrModemError::Busy;
    }

    SlrModemError rv = SlrModemError::Fail;
    if (xSemaphoreTake(xSLRSemaphore, xTicksToWait) == pdTRUE)
    {
        m_WriteString(SLR_GET_RSSI_LAST_RX_STRING);

        rv = m_WaitCmdResponse();

        if (rv == SlrModemError::Ok)
        {
            rv = m_HandleMessage_RS(pRssi);
        }
        xSemaphoreGive(xSLRSemaphore);
    }
    return rv;
}

SlrModemError SlrModem::GetRssiCurrentChannel(int16_t *pRssi)
{
    if (m_asyncExpectedResponse != SlrModemResponse::Idle)
    {
        return SlrModemError::Busy;
    }
    
    SlrModemError rv = SlrModemError::Fail;
    if (xSemaphoreTake(xSLRSemaphore, xTicksToWait) == pdTRUE)
    {
        m_WriteString(SLR_GET_RSSI_CURRENT_CHANNEL_STRING);

        rv = m_WaitCmdResponse();
        if (rv == SlrModemError::Ok)
        {
            rv = m_HandleMessage_RA(pRssi);
        }
        xSemaphoreGive(xSLRSemaphore);
    }
    return rv;
}

SlrModemError SlrModem::GetRssiCurrentChannelAsync()
{
    if (m_asyncExpectedResponse != SlrModemResponse::Idle)
    {
        return SlrModemError::Busy;
    }

    SlrModemError rv = SlrModemError::Fail;
    if (xSemaphoreTake(xSLRSemaphore, xTicksToWait) == pdTRUE)
    {
        m_WriteString(SLR_GET_RSSI_CURRENT_CHANNEL_STRING);
        m_asyncExpectedResponse = SlrModemResponse::RssiCurrentChannel;
        rv = SlrModemError::Ok;
        xSemaphoreGive(xSLRSemaphore);
    }
    return rv;
}

SlrModemError SlrModem::GetContactFunction(uint8_t *pContactFunction)
{
    if (m_asyncExpectedResponse != SlrModemResponse::Idle)
    {
        return SlrModemError::Busy;
    }

    SlrModemError rv = SlrModemError::Fail;
    if (xSemaphoreTake(xSLRSemaphore, xTicksToWait) == pdTRUE)
    {
        m_WriteString(SLR_GET_CONTACT_FUNCTION_STRING);

        rv = m_WaitCmdResponse();

        if (rv == SlrModemError::Ok)
        {
            rv = m_HandleMessageHexByte(pContactFunction, SLR_SET_CONTACT_FUNCTION_RESPONSE_LEN, SLR_SET_CONTACT_FUNCTION_RESPONSE_PREFIX);
        }
        xSemaphoreGive(xSLRSemaphore);
    }
    return rv;
}

SlrModemError SlrModem::SetContactFunction(uint8_t contactFunction, bool saveValue)
{
    if (m_asyncExpectedResponse != SlrModemResponse::Idle)
    {
        return SlrModemError::Busy;
    }

    SlrModemError rv = SlrModemError::Fail;
    if (xSemaphoreTake(xSLRSemaphore, xTicksToWait) == pdTRUE)
    {
        // e.g. @PS2D\r\n or @PS7F/W\r\n
        m_WriteString(SLR_SET_CONTACT_FUNCTION_PREFIX_STRING);
        std::array<char, 7> numStr;
        snprintf(numStr.data(), numStr.size(), "%02X%s\r\n", static_cast<unsigned>(contactFunction), (saveValue ? ("/W") : ("")));
        m_WriteString(numStr.data());

        SlrModemError rv = m_WaitCmdResponse();
        if (rv == SlrModemError::Ok && saveValue)
        {
            rv = m_HandleMessage_WR();
            if (rv == SlrModemError::Ok)
            {
                rv = m_WaitCmdResponse();
            }
        }

        uint8_t contactFunctionResponse{};
        if (rv == SlrModemError::Ok)
        {
            rv = m_HandleMessageHexByte(&contactFunctionResponse, SLR_SET_CONTACT_FUNCTION_RESPONSE_LEN, SLR_SET_CONTACT_FUNCTION_RESPONSE_PREFIX);
        }

        if (rv == SlrModemError::Ok && contactFunctionResponse != contactFunction)
        {
            rv = SlrModemError::Fail;
        }
        xSemaphoreGive(xSLRSemaphore);
    }
    return rv;
}

SlrModemError SlrModem::GetSerialNumber(uint32_t *pSerialNumber)
{
    if (m_asyncExpectedResponse != SlrModemResponse::Idle)
    {
        return SlrModemError::Busy;
    }

    SlrModemError rv = SlrModemError::Fail;
    if (xSemaphoreTake(xSLRSemaphore, xTicksToWait) == pdTRUE)
    {
        m_WriteString(SLR_GET_SERIAL_NUMBER_STRING);

        SlrModemError rv = m_WaitCmdResponse();
        if (rv == SlrModemError::Ok)
        {
            rv = m_HandleMessage_SN(pSerialNumber);
        }
        xSemaphoreGive(xSLRSemaphore);
    }
    return rv;
}

SlrModemError SlrModem::GetSerialNumberAsync()
{
    if (m_asyncExpectedResponse != SlrModemResponse::Idle)
    {
        return SlrModemError::Busy;
    }

    SlrModemError rv = SlrModemError::Fail;
    if (xSemaphoreTake(xSLRSemaphore, xTicksToWait) == pdTRUE)
    {
        m_WriteString(SLR_GET_SERIAL_NUMBER_STRING);
        m_asyncExpectedResponse = SlrModemResponse::SerialNumber;
        rv = SlrModemError::Ok;
        xSemaphoreGive(xSLRSemaphore);
    }
    return rv;
}

SlrModemError SlrModem::GetPacket(const uint8_t **ppData, uint8_t *len)
{
    if (m_drMessagePresent)
    {
        *ppData = &m_drMessage[0];
        *len = m_drMessageLen;
        return SlrModemError::Ok;
    }
    else
        return SlrModemError::Fail;
}

void SlrModem::Work()
{
    switch (m_Parse())
    {
    case SlrModemCmdState::Parsing:
        // nop
        break;

    case SlrModemCmdState::Garbage:
        if (m_pCallback)
        {
            // Garbage
        }
        break;
    case SlrModemCmdState::Overflow:
        if (m_pCallback)
        {
            // Overflow
        }
        break;
    case SlrModemCmdState::FinishedCmdResponse:
        m_DispatchCmdResponseAsync();
        break;
    case SlrModemCmdState::FinishedDrResponse:
        if (m_pCallback)
        {
            // FinishedDrResponse
            m_pCallback(SlrModemError::Ok, SlrModemResponse::DataReceived, 0, &m_drMessage[0], m_drMessageLen);
        }
        break;
    }
}

void SlrModem::m_WriteString(const char *pString)
{
    size_t len = strlen(pString);
    m_pUart->write(reinterpret_cast<const uint8_t *>(pString), len);
}

uint8_t SlrModem::m_ReadByte()
{
    if (m_oneByteBuf != -1)
    {
        uint8_t rv = static_cast<uint8_t>(m_oneByteBuf);
        m_oneByteBuf = -1;
        return rv;
    }

    uint8_t rcv = m_pUart->read();

    return rcv;
}

void SlrModem::m_UnreadByte(uint8_t unreadByte)
{
    m_oneByteBuf = unreadByte;
}

void SlrModem::m_ClearUnreadByte()
{
    m_oneByteBuf = -1;
}

uint32_t SlrModem::m_Read(uint8_t *pDst, uint32_t count)
{
    if (m_oneByteBuf != -1)
    {
        *pDst++ = static_cast<uint8_t>(m_oneByteBuf);
        m_oneByteBuf = -1;
        --count;
    }

    return m_pUart->readBytes(pDst, count);
}

void SlrModem::m_ResetParser()
{
    m_parserState = SlrModemParserState::Start;
    m_ClearUnreadByte();
}

void SlrModem::m_ClearOneLine()
{
    m_pUart->setTimeout(500);
    m_pUart->readStringUntil('\n');
}

void SlrModem::m_FlushGarbage()
{
    // remove all remaining garbage from the pipeline, except '*' implies a valid message will follow
    // don't care about special cases
    if (m_oneByteBuf == -1)
    {
        while (m_pUart->available())
        {
            if ('*' == m_ReadByte())
            {
                m_UnreadByte('*');
                break;
            }
        }
    }
    m_parserState = SlrModemParserState::Start;
}

SlrModemCmdState SlrModem::m_Parse()
{
    while (m_pUart->available())
    {
        switch (m_parserState)
        {
        case SlrModemParserState::Start:
            m_rxIdx = 0;
            m_rxMessage[m_rxIdx] = m_ReadByte();

            if (m_rxMessage[m_rxIdx] == '*')
            {
                ++m_rxIdx;
                m_parserState = SlrModemParserState::ReadCmdFirstLetter;
            }
#if 0 
            // TODO discuss whether we want to read the non CMD-Formatted messages, or just delete them silently
            else if (isupper(m_rxMessage[m_rxIdx]))
            {
               ++m_rxIdx;
               m_parserState = SlrModemParserState::ReadRawString;
            }
#endif
            else // garbage
            {
                m_FlushGarbage();
                // clearing the pipeline is not reported as error
                return SlrModemCmdState::Parsing;
            }
            break;

        case SlrModemParserState::ReadCmdFirstLetter:
            m_rxMessage[m_rxIdx] = m_ReadByte();

            if (isupper(m_rxMessage[m_rxIdx]))
            {
                ++m_rxIdx;
                m_parserState = SlrModemParserState::ReadCmdSecondLetter;
            }
            else
            {
                if (m_rxMessage[m_rxIdx] == '*') // two '*' in a row, ignore first
                {
                    m_UnreadByte('*');
                }
                m_FlushGarbage();
                return SlrModemCmdState::Garbage;
            }
            break;

        case SlrModemParserState::ReadCmdSecondLetter:
            m_rxMessage[m_rxIdx] = m_ReadByte();

            if (isupper(m_rxMessage[2]))
            {
                ++m_rxIdx;
                m_parserState = SlrModemParserState::ReadCmdParam;
            }
            else
            {
                if (m_rxMessage[2] == '*')
                {
                    m_UnreadByte('*'); // keep unexpected '*' for next message
                }
                m_FlushGarbage();
                return SlrModemCmdState::Garbage;
            }
            break;

        case SlrModemParserState::ReadCmdParam:
            m_rxMessage[m_rxIdx] = m_ReadByte();

            if ((m_rxMessage[1] == 'D') && (m_rxMessage[2] == 'R') && (m_rxMessage[3] == '='))
            {
                // @DR telegram
                ++m_rxIdx;
                m_parserState = SlrModemParserState::RadioDrSize;
            }
            else if (isupper(m_rxMessage[1]) && isupper(m_rxMessage[2]) && (m_rxMessage[3] == '=')) // TODO check '=' is always part of the response
            {
                // some other type of telegram that appears consistent
                ++m_rxIdx;
                m_parserState = SlrModemParserState::ReadCmdUntilCR;
            }
            else
            {
                if (m_rxMessage[m_rxIdx] == '*') // another '*', ignore the three read characters
                {
                    m_UnreadByte('*');
                }
                m_FlushGarbage();
                return SlrModemCmdState::Garbage;
            }
            break;

        case SlrModemParserState::RadioDrSize:
            m_rxMessage[m_rxIdx] = m_ReadByte();
            ++m_rxIdx;
            if (m_rxIdx < 6)
            {
                // parser cannot continue until there are at least two characters
                return SlrModemCmdState::Parsing;
            }

            if (isxdigit(m_rxMessage[4]) && isxdigit(m_rxMessage[5]))
            {
                m_drMessagePresent = false;
                uint32_t msgLen = 0;
                s_ParseHex(&m_rxMessage[4], 2, &msgLen);
                m_drMessageLen = msgLen;
                m_rxIdx = 0; // now m_rxIdx to m_drMessage Buffer
#ifdef SLRMODEM_SKIP_ADRESS_FOR_MLR
                m_drMessage[0] = 0;
                m_parserState = SlrModemParserState::RadioDrSkipAdress;

#else
                m_parserState = SlrModemParserState::RadioDrPayload;
#endif
            }
            else
            {
                m_FlushGarbage();
                return SlrModemCmdState::Garbage;
            }
            break;

#ifdef SLRMODEM_SKIP_ADRESS_FOR_MLR
        case SlrModemParserState::RadioDrSkipAdress:
        {
            m_Read(&m_drMessage[1], 1);
            --left;

            if (m_drMessage[1] == ':')
            {
                if (m_drMessage[0] == 0)
                    m_drMessage[0] = 1;

                else
                    m_parserState = SlrModemParserState::RadioDrPayload;
            }
        }
        break;
#endif

        case SlrModemParserState::RadioDrPayload:
        {
            // example DR:
            // *DR=05hallo\r\n

            m_drMessage[m_rxIdx] = m_ReadByte();
            ++m_rxIdx;

            if ((m_drMessageLen + 2 - m_rxIdx) == 0)
            {
                if ((m_drMessage[m_rxIdx - 2] == '\r') && m_drMessage[m_rxIdx - 1] == '\n')
                {
                    m_drMessage[m_rxIdx - 2] = 0;//set null at end of the message
                    m_rxIdx = 0;
                    m_rxMessage[0] = 0; // "destroy" the old CMD message, so nobody will expect the new message to be a regular command response instead of a radio packet
                    m_drMessagePresent = true;
                    m_parserState = SlrModemParserState::Start;
                    return SlrModemCmdState::FinishedDrResponse;
                }
                else
                {
                    m_FlushGarbage();
                    return SlrModemCmdState::Garbage;
                }
            }
            
            break;
        }

        case SlrModemParserState::ReadCmdUntilCR:
            m_rxMessage[m_rxIdx] = m_ReadByte();

            if (m_rxMessage[m_rxIdx] == '\r')
            {
                ++m_rxIdx;
                if (m_rxIdx == sizeof(m_rxMessage))
                {
                    m_parserState = SlrModemParserState::Start;
                    return SlrModemCmdState::Overflow;
                }
                else
                {
                    m_parserState = SlrModemParserState::ReadCmdUntilLF;
                }
            }
            else if (m_rxMessage[m_rxIdx] == '\n') // unexpected end of command, reset parser
            {
                m_FlushGarbage();
                return SlrModemCmdState::Garbage;
            }
            else if (m_rxMessage[m_rxIdx] == '*') // another '*', ignore already read characters
            {
                m_UnreadByte('*');
                m_FlushGarbage();
                return SlrModemCmdState::Garbage;
            }
            else
            {
                ++m_rxIdx;
                if (m_rxIdx == sizeof(m_rxMessage))
                {
                    m_parserState = SlrModemParserState::Start;
                    return SlrModemCmdState::Overflow;
                }
            }
            break;

        case SlrModemParserState::ReadCmdUntilLF:
            m_rxMessage[m_rxIdx] = m_ReadByte();
            if (m_rxMessage[m_rxIdx] == '\n')
            {
                // decrease index pointer to compensate for increasing with CR;
                // CR\LF is not considered part of message
                --m_rxIdx;
                m_parserState = SlrModemParserState::Start;
                return SlrModemCmdState::FinishedCmdResponse;
            }
            else // garbage
            {
                if (m_rxMessage[m_rxIdx] == '*') // another '*', ignore already read characters
                {
                    m_UnreadByte('*');
                }
                m_FlushGarbage();
                return SlrModemCmdState::Garbage;
            }
            break;

        default:
            // this should never be reached
            m_parserState = SlrModemParserState::Start;
            m_rxIdx = 0;
            break;
        }
    }

    return SlrModemCmdState::Parsing;
}

SlrModemError SlrModem::m_WaitCmdResponse(uint32_t ms)
{
    // We might just receiving a Dr Telegram, when sending a normal command to the modem.
    // Thus while waiting for the command response, receiving a Dr message must be taken into account.
    m_StartTimeout(ms);
    while (!m_IsTimeout())
    {
        switch (m_Parse())
        {
        case SlrModemCmdState::Parsing:
            // nothing to do
            break;

        case SlrModemCmdState::FinishedCmdResponse:
            return SlrModemError::Ok;
            break;

        case SlrModemCmdState::FinishedDrResponse:
            if (m_pCallback)
            {
                m_pCallback(SlrModemError::Ok, SlrModemResponse::DataReceived, 0, m_drMessage, m_drMessageLen);
            }
            break;

        default:
            return SlrModemError::Fail;
        }

        vTaskDelay(10 / portTICK_PERIOD_MS); //To switch to other task
    }
    m_parserState = SlrModemParserState::Start;
    return SlrModemError::Fail;
}

void SlrModem::m_SetExpectedResponses(SlrModemResponse ep0, SlrModemResponse ep1, SlrModemResponse ep2)
{
    m_asyncExpectedResponses[0] = ep0;
    m_asyncExpectedResponses[1] = ep1;
    m_asyncExpectedResponses[2] = ep2;
}

SlrModemError SlrModem::m_DispatchCmdResponseAsync()
{
    SlrModemError err = SlrModemError::Fail;

    switch (m_asyncExpectedResponse)
    {
    case SlrModemResponse::Idle:
        // TODO logic error
        break;
    case SlrModemResponse::ParseError:
        // TODO
        break;
    case SlrModemResponse::Timeout:
        // TODO
        break;
    case SlrModemResponse::ShowMode:
        break;
    case SlrModemResponse::SaveValue:
        break;
    case SlrModemResponse::Channel:
        break;
    case SlrModemResponse::SerialNumber:
        break;
    case SlrModemResponse::SlrModem_DtAck:
        break;
    case SlrModemResponse::SlrModem_DtIr:
        break;
    case SlrModemResponse::DataReceived:
        break;
    case SlrModemResponse::RssiLastRx:
        break;
    case SlrModemResponse::RssiCurrentChannel:
        if (m_pCallback)
        {
            int16_t rssi{};
            err = m_HandleMessage_RA(&rssi);
            m_pCallback(err, SlrModemResponse::RssiCurrentChannel, static_cast<int32_t>(rssi), nullptr, 0);
        }
        break;
    default:
        break;
    }

    m_asyncExpectedResponse = SlrModemResponse::Idle;
    return err;
}

SlrModemError SlrModem::m_HandleMessage_WR()
{
    uint16_t messageLen = m_rxIdx;

    if ((messageLen == SLR_WRITE_VALUE_RESPONSE_LEN) && !strncmp(SLR_WRITE_VALUE_RESPONSE_PREFIX, (char *)&m_rxMessage[0], SLR_WRITE_VALUE_RESPONSE_LEN))
    {
        return SlrModemError::Ok;
    }

    return SlrModemError::Fail;
}

SlrModemError SlrModem::m_GetHexByte(uint8_t *pValue, const char *commandString, uint32_t responseLen, const char *responsePrefix)
{
    return SlrModemError::Fail;
}

SlrModemError SlrModem::m_HandleMessageHexByte(uint8_t *pValue, uint32_t responseLen, const char *responsePrefix)
{
    uint16_t messageLen = m_rxIdx;
    if (messageLen != responseLen)
    {
        return SlrModemError::Fail; // message wrong size
    }

    uint32_t responsePrefixLen = strlen(responsePrefix);
    uint16_t channelHexIndex = responsePrefixLen;
    if (!strncmp(responsePrefix, (char *)&m_rxMessage[0], responsePrefixLen))
    {
        uint32_t value{};
        if (s_ParseHex(&m_rxMessage[channelHexIndex], 2, &value))
        {
            *pValue = value;
            return SlrModemError::Ok;
        }

        return SlrModemError::Fail;
    }

    return SlrModemError::Fail;
}

SlrModemError SlrModem::m_HandleMessage_RS(int16_t *pRssi)
{
    uint16_t messageLen = m_rxIdx;
    if ((messageLen < SLR_GET_RSSI_LAST_RX_RESPONSE_MIN_LEN) || (messageLen > SLR_GET_RSSI_LAST_RX_RESPONSE_MAX_LEN))
    {
        return SlrModemError::Fail; // message wrong size
    }

    uint16_t responsePrefixLen = static_strlen(SLR_GET_RSSI_LAST_RX_RESPONSE_PREFIX);
    if (!strncmp(SLR_GET_RSSI_LAST_RX_RESPONSE_PREFIX, (char *)&m_rxMessage[0], responsePrefixLen))
    {
        // check the last chars to be "dBm"
        if ((m_rxMessage[messageLen - 3] == 'd') &&
            (m_rxMessage[messageLen - 2] == 'B') &&
            (m_rxMessage[messageLen - 1] == 'm'))
        {
            m_rxMessage[messageLen - 3] = 0; // cut the "d" which will make the number string a zero terminated string
            uint8_t *pEnd;
            long result = strtol((char *)&m_rxMessage[4], (char **)&pEnd, 10);

            if (pEnd != &m_rxMessage[messageLen - 3])
            {
                // indicating the parsing stopped early because some char was not a number digit
                return SlrModemError::Fail;
            }

            *pRssi = (int16_t)result;
            return SlrModemError::Ok;
        }

        return SlrModemError::Fail;
    }

    return SlrModemError::Fail;
}

// check if the received message is of RA format and fill the RSSI
SlrModemError SlrModem::m_HandleMessage_RA(int16_t *pRssi)
{
    uint16_t messageLen = m_rxIdx;
    if ((messageLen < SLR_GET_RSSI_CURRENT_CHANNEL_RESPONSE_MIN_LEN) || (messageLen > SLR_GET_RSSI_CURRENT_CHANNEL_RESPONSE_MAX_LEN))
    {
        return SlrModemError::Fail; // message wrong size
    }

    uint16_t responsePrefixLen = static_strlen(SLR_GET_RSSI_CURRENT_CHANNEL_RESPONSE_PREFIX);
    if (!strncmp(SLR_GET_RSSI_CURRENT_CHANNEL_RESPONSE_PREFIX, (char *)&m_rxMessage[0], responsePrefixLen))
    {
        // check the last chars to be "dBm"
        if ((m_rxMessage[messageLen - 3] == 'd') &&
            (m_rxMessage[messageLen - 2] == 'B') &&
            (m_rxMessage[messageLen - 1] == 'm'))
        {
            m_rxMessage[messageLen - 3] = 0; // cut the "d" which will make the number string a zero terminated string
            uint8_t *pEnd;
            long result = strtol((char *)&m_rxMessage[4], (char **)&pEnd, 10);

            if (pEnd != &m_rxMessage[messageLen - 3])
            {
                // indicating the parsing stopped early because some char was not a number digit
                return SlrModemError::Fail;
            }

            *pRssi = (int16_t)result;
            return SlrModemError::Ok;
        }

        return SlrModemError::Fail;
    }

    return SlrModemError::Fail;
}

SlrModemError SlrModem::m_HandleMessage_SN(uint32_t *pSerialNumber)
{
    uint16_t messageLen = m_rxIdx;

    uint16_t responsePrefixLen = static_strlen(SLR_GET_SERIAL_NUMBER_RESPONSE_PREFIX);
    if ((messageLen == SLR_GET_SERIAL_NUMBER_RESPONSE_LEN) && !strncmp(SLR_GET_SERIAL_NUMBER_RESPONSE_PREFIX, (char *)&m_rxMessage[0], responsePrefixLen))
    {
        uint32_t serialNumber{};

        // maybe leading "S" ahead of hex code, take into account! (*SN=S0000001 vs *SN=00000001)
        uint8_t startIdx = 4;
        uint8_t hexLen = 8;
        if (!isdigit( m_rxMessage[4]))
        {
            startIdx = 5;
            hexLen = 7;
        }

        if (!s_ParseDec(&m_rxMessage[startIdx], hexLen, &serialNumber))
        {
            return SlrModemError::Fail;
        }
        else
        {
            if (pSerialNumber)
            {
                *pSerialNumber = serialNumber;
            }

            return SlrModemError::Ok;
        }
    }

    return SlrModemError::Fail;
}
