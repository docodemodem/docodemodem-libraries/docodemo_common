/*
 * AD converter driver for MCP3221
 * mcp3221.cpp
 *
 * Copyright (c) 2021 Circuit Desgin,Inc
 * Released under the MIT license
 */

#include "mcp3221.h"

MCP3221::MCP3221(void){
}

bool MCP3221::begin(uint8_t addr, TwoWire &theWire, float Vref)
{
    i2c_addr = addr;
    _wire = &theWire;
    _Vref = Vref;

    return true;
}

float MCP3221::readADC()
{
	int16_t data=0;
	
    _wire->requestFrom(i2c_addr, (uint8_t)2);
	
	if(_wire->available() == 2){
		data = (_wire->read() << 8) + _wire->read();
	}
	
    return (float)data * _Vref / 4096.0;
}
