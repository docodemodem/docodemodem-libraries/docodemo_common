/*
 * AD converter driver
 * mcp3221.h
 *
 * Copyright (c) 2021 Circuit Desgin,Inc
 * Released under the MIT license
 */

#ifndef _MCP3221_H_
#define _MCP3221_H_

#include "Board.h"

class MCP3221{
public:
    MCP3221();
    bool begin(uint8_t addr = MCP3221_I2C_ADD, TwoWire &theWire = Wire, float Vref = 3.0);
    float readADC();
private:
    float _Vref;
    TwoWire *_wire;
    uint8_t i2c_addr;
};
#endif