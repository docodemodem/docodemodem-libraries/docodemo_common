/*
 * AD converter driver
 * mcp3425.h
 *
 * Copyright (c) 2020 Circuit Desgin,Inc
 * Released under the MIT license
 */

#ifndef _MCP3425_H_
#define _MCP3425_H_

#include "Board.h"

class MCP3425{
public:
    const float Vref = 2.048;
    MCP3425();
    bool begin(uint8_t addr = MCP3425_I2C_ADD, TwoWire &theWire = Wire);
    float readADC(uint8_t channel = 0);
    void startMeasure(uint8_t channel = 0);
    float readResult();
private:
    TwoWire *_wire;
    uint8_t i2c_addr;
};
#endif